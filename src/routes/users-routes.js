import React, { Component } from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom';
import SinglePageLayout  from'../layouts/singlePageLayout'
import LoginFormContainer from '../components/login-form/login-form-container';
import SignUpFormContainer from '../components/signup-form/signup-form-container'
import ForgetPasswordContainer from '../components/forget-password/forget-password-container'
import HomeContainer from '../components/home/home-container';
export default class UsersRoute extends Component {
    constructor(props) {
        super(props)
        this.state = {
             isUserLogin:true
        }
    }
    
    render(props) {
        return (
               <Router>
                   <Route exact path='/' render={(props)=>{return this.state.isUserLogin?<SinglePageLayout children={HomeContainer} {...props}/>:<LoginFormContainer {...props}/>}}/>
                   <Route exact path="/forgetpassword" render={(props)=>{return(<ForgetPasswordContainer {...props}/>)}}/>
                   <Route exact path="/signup" render={(props)=>{return(<SignUpFormContainer {...props}/>)}}/>
               </Router>     
        )
    }
}

import React, { Component } from 'react'
import { NavLink, Link } from 'react-router-dom';
import SidebarComponent from '../components/sidebar/sidebar-container';
export default class SinglePageLayout extends Component {
    render() {
        return (
            <React.Fragment>
                <main className="main-wrapper">
                    <SidebarComponent {...this.props} />

                    <div className="content">
                        <header className="header text-right">
                            <a href="" className="facebook">
                                <i className="fab fa-facebook-f"></i>
                            </a>
                            <a href="" className="twitter">
                                <i className="fab fa-twitter"></i>
                            </a>
                            <a href="#" className="linkedin">
                                <i className="fab fa-linkedin-in"></i>
                            </a>
                            <a href="#" className="instagram">
                                <i className="fab fa-instagram" />
                            </a>
                        </header>
                        {/*header content header end*/}
                        <this.props.children {...this.props} />
                    </div>
                    {/* content end */}
                </main>
                {/* main wrapper end*/}
            </React.Fragment>
        )
    }
}

import React, { Component } from 'react'
import LoginFormComponent from './login-form-component';
import {HandlePageOnCLick} from '../../web-utils'
import $ from 'jquery'
export default class LoginFormContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userEmail:'',
            userPassword:'' ,
            isPassVisible:false
        }
    }
    isWhitespace = (s) => {
        //alert(s.length);
       let i;
       let whitespace = " \t\n\r";
       // Is s empty?
       if (this.isEmpty(s)) return true;

       // Search through string's characters one by one
       // until we find a non-whitespace character.
       // When we do, return false; if we don't, return true.

       for (i = 0; i < s.length; i++) {
           // Check that current character isn't whitespace.

           // alert(s.charAt(1));

           // if (s.charAt(0) === ' ') return false;
           // alert(s.charAt(i));
           let c = s.charAt(i);
           if (whitespace.indexOf(c) === -1) return false;
       }

       // All characters are whitespace.
       return true;
   }

   isEmpty = (s)=> {
       return ((s == null) || (s.length == 0) || (s == ''))
   }
    HandleShowOrHidePass =()=>{
            let tmpVal = ! this.state.isPassVisible;
            this.setState({
                isPassVisible:tmpVal
            })
    }
    HandleValueOnChange = (e)=>{
         let name = e.target.id;
         let val = e.target.value;
         if(name ==="userEmail"){
            let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (!(val.match(mailformat))) {
                $('#spnErr_txtUserEmail').text('Please Enter a vailid email').css({
                    color: 'red',
                    fontSize: '12px',
                    position: 'relative',
                    top: '0px'
                }).show();
            }
            else {
               
                $('#spnErr_txtUserEmail').text('').hide();
                this.setState({
                    [name]:val
                })
            }
         }
         this.setState({
            [name]:val
        })
         
     }
    HandleOnSubmit=()=>{
            console.log(this.state);
    } 
    render() {
        return (
           <LoginFormComponent
            HandleValueOnChange={this.HandleValueOnChange}
            HandleOnSubmit={this.HandleOnSubmit}
            HandleShowOrHidePass={this.HandleShowOrHidePass}
            state={this.state} 
            {...this.props}
            />
        )
    }
}

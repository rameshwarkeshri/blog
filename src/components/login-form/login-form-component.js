import React, { Component } from 'react'
import {HandlePageOnCLick} from '../../web-utils'
export default class LoginFormComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="s-form-wrapper">
                  <div className="s-form-left">

                  </div>
                     {/* form left sec */}
                   <div className="s-form-right">
                        <form className="s-login-form" id="loginForm">
                            <h2 className="s-form-title">
                                Login Here
                            </h2>
                            <div className="s-form-group">
                                <input type="email"
                                id="userEmail" 
                                name="userEmail" 
                                value={this.props.state.userEmail} 
                                className={this.props.state.userEmail?"s-form-control has-input":"s-form-control"}
                                onChange={this.props.HandleValueOnChange}/>
                                <label htmlFor="userEmail" className="s-form-label">Email</label>
                            </div>
                            <div className="s-error-text" id="spnErr_txtUserEmail"></div>
                            {/* form group end 1 */}
                            <div className="s-form-group">
                                <input type={this.props.state.isPassVisible?"text":'password'}
                                id="userPassword"
                                name="userPassword"
                                value={this.props.state.userPassword}
                                className={this.props.state.userPassword?"s-form-control has-input password":"s-form-control password"}
                                onChange={this.props.HandleValueOnChange}/>
                                <span className={this.props.state.isPassVisible?"show-hide-icon show":"show-hide-icon"}
                                onClick={this.props.HandleShowOrHidePass}
                                ></span>
                                <label htmlFor="userPassword" className="s-form-label">Password</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid password</div>
                            {/* form group end 2 */}
                            <div className='s-form-group'>
                                <button type="button" className="s-btn-login" onClick={this.props.HandleOnSubmit}>
                                    Login
                                </button>
                               <div className="s-form-link-wrapper">
                                    <p>
                                        <a onClick={()=>HandlePageOnCLick(this.props,'/signup')} className="s-btn-link">Signup</a>
                                    </p>
                                   <p>
                                        <a onClick={()=>HandlePageOnCLick(this.props,'/forgetpassword')} className="s-btn-link">Forget Password?</a>
                                   </p>                             
                                
                               </div>
                            </div>
                        </form>
                   </div>{/* form right sec */}  
               
                </div>
            </React.Fragment>
        )
    }
}

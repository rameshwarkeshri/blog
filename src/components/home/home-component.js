import React, { Component } from 'react';
import { Link } from 'react-router-dom';
const text = ` Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla quod nihil, nam labore pariatur facilis distinctio, cupiditate laborum, consequuntur odio minus debitis. Debitis qui officia, rem quod explicabo consequatur expedita!
Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla quod nihil, nam labore pariatur facilis distinctio, cupiditate laborum, consequuntur odio minus debitis. Debitis qui officia, rem quod explicabo consequatur expedita!
Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla quod nihil, nam labore pariatur facilis distinctio, cupiditate laborum, consequuntur odio minus debitis. Debitis qui officia, rem quod explicabo consequatur expedita!
Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla quod nihil, nam labore pariatur facilis distinctio, cupiditate laborum, consequuntur odio minus debitis. Debitis qui officia, rem quod explicabo consequatur expedita!
Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla quod nihil, nam labore pariatur facilis distinctio, cupiditate laborum, consequuntur odio minus debitis. Debitis qui officia, rem quod explicabo consequatur expedita!`
export default class HomeComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="content-body">
                    <div className="left-sec">
                        <div className="blog-card-wrapper">
                            <div className="card-img">
                                <img src={require('../../assets/img/login-bg1.jpg')} />
                            </div>
                            {/* card img end */}
                            <div className="card-details">
                                <h1 className="blog-title">
                                    Talking cricket? Bring up the women’s team !
                                </h1>
                                <p className="blog-para mb-0">
                                    {text.substring(0,300) +"..."}
                                    <a href="#">
                                        ReadMore
                                    </a>
                                </p>
                            </div>
                            {/* card details end */}
                        </div>
                        {/* card wrapper end */}
                    </div>
                    {/* left sec end*/}
                    <div className="right-sec">
                        <div className="right-content-wrapper">
                        </div>
                        {/* content wrapper end */}
                    </div>
                    {/* right sec end */}

                </div>
                {/*Content body end*/}

            </React.Fragment>
        )
    }
}

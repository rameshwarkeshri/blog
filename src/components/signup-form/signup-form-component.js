import React, { Component } from 'react'
import {HandlePageOnCLick} from '../../web-utils'
export default class SignUpFormComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="s-form-wrapper">
                  <div className="s-form-left">

                  </div>
                     {/* form left sec */}
                   <div className="s-form-right">
                        <form className="s-login-form" id="loginForm">
                            <h2 className="s-form-title">
                                Signup Here
                            </h2>
                            <div className="s-form-group">
                                <input type="text"
                                id="userFirstName" 
                                name="userFirstName" 
                                value={this.props.state.userFirstName} 
                                className={this.props.state.userFirstName?"s-form-control has-input":"s-form-control"}
                                onChange={this.props.HandleValueOnChange}/>
                                <label htmlFor='userFirstName' className="s-form-label">First Name</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid name</div>
                            {/* form group end 1 */}
                            <div className="s-form-group">
                                <input type="text"
                                id="userLastName" 
                                name="userLastName" 
                                value={this.props.state.userLastName} 
                                className={this.props.state.userLastName?"s-form-control has-input":"s-form-control"}
                                onChange={this.props.HandleValueOnChange}/>
                                <label htmlFor='userLastName' className="s-form-label">Last Name</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid name</div>
                            {/* form group end 2 */}
                            <div className="s-form-group">
                                <input type="text"
                                id="userNo" 
                                name="userNo" 
                                value={this.props.state.userNo} 
                                className={this.props.state.userNo?"s-form-control has-input":"s-form-control"}
                                onChange={this.props.HandleValueOnChange}/>
                                <label htmlFor='userNo' className="s-form-label">Phone Number</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid name</div>
                            {/* form group end 3 */}
                            <div className="s-form-group">
                                <input type="email"
                                id="userEmail" 
                                name="userEmail" 
                                value={this.props.state.userEmail} 
                                className={this.props.state.userEmail?"s-form-control has-input":"s-form-control"}
                                onChange={this.props.HandleValueOnChange}/>
                                
                                <label htmlFor='userEmail'  className="s-form-label">Email</label>
                            </div>
                            <div className="s-error-text" id="spnErr_txtUserEmail"></div>
                            {/* form group end 1 */}
                            <div className="s-form-group">
                                <input 
                                type={this.props.state.isPassVisible?"text":'password'} 
                                id="userPassword"
                                name="userPassword"
                                value={this.props.state.userPassword}
                                className={this.props.state.userPassword?"s-form-control has-input":"s-form-control"}
                                onChange={this.props.HandleValueOnChange}/>
                                <span className={this.props.state.isPassVisible?"show-hide-icon show":"show-hide-icon"}
                                onClick={()=>this.props.HandleShowOrHidePass('isPassVisible')}
                                ></span>
                                <label htmlFor="userPassword" className="s-form-label">Password</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid password</div>
                            {/* form group end 2 */}
                            <div className="s-form-group">
                                <input type={this.props.state.isReTypePassVisible?"text":'password'} 
                                id="userReTypePassword"
                                name="userReTypePassword"
                                value={this.props.state.userReTypePassword}
                                className={this.props.state.userReTypePassword?"s-form-control has-input":"s-form-control"}
                                onChange={this.props.HandleValueOnChange}/>
                                <span className={this.props.state.isReTypePassVisible?"show-hide-icon show":"show-hide-icon"}
                                onClick={()=>this.props.HandleShowOrHidePass('isReTypePassVisible')}
                                ></span>
                                <label htmlFor="userReTypePassword" className="s-form-label">Re Type Password</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid password</div>
                            {/* form group end 2 */}
                            <div className='s-form-group'>
                                <button type="button" className="s-btn-login" onClick={this.props.HandleOnSubmit}>
                                    Signup
                                </button>
                                <p>
                                        <a href="" onClick={()=>HandlePageOnCLick(this.props,'/')} className="s-btn-link">Login</a>
                                </p>
                            </div>
                        </form>
                   </div>{/* form right sec */}  
               
                </div>
            </React.Fragment>
        )
    }
}

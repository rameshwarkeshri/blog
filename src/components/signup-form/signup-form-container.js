import React, { Component } from "react";
import SignUpFormComponent from "./signup-form-component";
import { HandlePageOnCLick } from "../../web-utils";
import $ from "jquery";
export default class SignUpFormContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: "",
      userPassword: "",
      userReTypePassword: "",
      userNo: "",
      userFirstName: "",
      userLastName: "",
      isPassVisible: false,
      isReTypePassVisible: false,
    };
  }
  HandleShowOrHidePass = (name) => {
    let tmpVal = !this.state[name];
    this.setState({
      [name]: tmpVal,
    });
  };
  HandleValueOnChange = (e) => {
    let name = e.target.name;
    let val = e.target.value;
    console.log("name=======" + name);
    if (name == "userEmail") {        
      let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (!val.match(mailformat)) {         
        $("#spnErr_txtUserEmail")
          .text("Please Enter a vailid email")
          .css({
            color: "red",
            fontSize: "12px",
            position: "relative",
            top: "0px",
          })
        //   .show();
      } else {
        
        $("#spnErr_txtUserEmail").text("").hide();
        this.setState({
          [name]: val,
        });
      }
    } else {
      this.setState({
        [name]: val,
      });
    }
  };
  HandleOnSubmit = () => {
    console.log(this.state);
  };
  render() {
    return (
      <SignUpFormComponent
        HandleValueOnChange={this.HandleValueOnChange}
        HandleOnSubmit={this.HandleOnSubmit}
        HandleShowOrHidePass={this.HandleShowOrHidePass}
        state={this.state}
        {...this.props}
      />
    );
  }
}

import React, { Component } from 'react'
import SidebarComponent from './sidebar-component';
export default class SidebarContainer extends Component {
    render() {
        return (
            <SidebarComponent {...this.props}/>
        )
    }
}

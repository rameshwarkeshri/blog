import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {NavLink,Link} from 'react-router-dom';
export default class SidebarComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <input type="checkbox" id="sidebarCheckbox" />
                <div className="sidebar">
                    <label className="toggle-btn" htmlFor="sidebarCheckbox">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    {/* toggle btn end*/}
                    <Link to="/" className="logo-link">
                        Best Blog
                        </Link>
                    {/* logo link */}
                    <ul className="sidebar-link-wrapper">
                        <li>
                            <NavLink to="/" activeClassName='active'>
                                Home
                                    </NavLink>
                            {/* home link */}
                            <NavLink to="/dailynews" activeClassName='active'>
                                Daily News
                                    </NavLink>
                            {/* Daily News link */}
                            <NavLink to="/dailynews" activeClassName='active'>
                                Trending
                                    </NavLink>
                            {/* Trending link */}
                            <NavLink to="/dailynews" activeClassName='active'>
                                Notes
                                    </NavLink>
                            {/* Notes link */}
                            <NavLink to="aboutus">
                                About us
                                    </NavLink>
                            {/* about us link */}
                            <NavLink to="aboutus">
                                Contact us
                                    </NavLink>
                            {/* contact us link */}
                        </li>
                    </ul>
                    {/* search box start*/}
                    <div className="search-box-wrapper">
                        <input type="text" placeholder="Search..." className="search-input" id="txtSearchInput" autoComplete="off" />
                        <button type="button" className="btn-search">
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                    {/* search box end*/}

                </div>
                {/*sidebar end*/}
            </React.Fragment>
        )
    }
}

import React, { Component } from 'react'
import ForgetPasswordComponent from './forget-password-component';
import {HandlePageOnCLick} from '../../web-utils'
export default class ForgetPasswordContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userPassword:'',
            reTypePassword:'',
            isPassVisible:false,
            isReTypePassVisible:false
        }
    }
    HandleShowOrHidePass =(name)=>{
            let tmpVal = ! this.state[name];
            this.setState({
                [name]:tmpVal
            })
    }
    HandleValueOnChange = (e)=>{
         let name = e.target.id;
         let val = e.target.value;
         this.setState({
             [name]:val
         })
     }
    HandleOnSubmit=()=>{
            console.log(this.state);
    } 
    render() {
        return (
           <ForgetPasswordComponent
            HandleValueOnChange={this.HandleValueOnChange}
            HandleOnSubmit={this.HandleOnSubmit}
            HandleShowOrHidePass={this.HandleShowOrHidePass}
            state={this.state} 
            {...this.props}
            />
        )
    }
}

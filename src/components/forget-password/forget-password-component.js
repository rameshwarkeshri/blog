import React, { Component } from 'react'
import {HandlePageOnCLick} from '../../web-utils'
export default class ForgetPasswordComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="s-form-wrapper">
                  <div className="s-form-left">

                  </div>
                  {/* form left sec */}
                   <div className="s-form-right">
                        <form className="s-login-form" id="loginForm">
                            <h2 className="s-form-title">
                                Reset Password
                            </h2>
                            <div className="s-form-group">
                                <input type={this.props.state.isPassVisible?"text":'password'}
                                id="userPassword"
                                name="userPassword"
                                value={this.props.state.userPassword}
                                className={this.props.state.userPassword?"s-form-control has-input password":"s-form-control password"}
                                onChange={this.props.HandleValueOnChange}/>
                                <span className={this.props.state.isPassVisible?"show-hide-icon show":"show-hide-icon"}
                                onClick={()=>this.props.HandleShowOrHidePass('isPassVisible')}
                                ></span>
                                <label htmlFor="userPassword" className="s-form-label">New Password</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid password</div>
                            {/* form group end 1 */}
                            <div className="s-form-group">
                                <input type={this.props.state.isReTypePassVisible?"text":'password'}
                                id="reTypePassword"
                                name="reTypePassword"
                                value={this.props.state.reTypePassword}
                                className={this.props.state.reTypePassword?"s-form-control has-input password":"s-form-control password"}
                                onChange={this.props.HandleValueOnChange}/>
                                <span className={this.props.state.isReTypePassVisible?"show-hide-icon show":"show-hide-icon"}
                                onClick={()=>this.props.HandleShowOrHidePass('isReTypePassVisible')}
                                ></span>
                                <label htmlFor="reTypePassword" className="s-form-label">Retype Password</label>
                            </div>
                            <div className="s-error-text">Please Enter a vailid password</div>
                            {/* form group end 2 */}
                            <div className='s-form-group'>
                                <button type="button" className="s-btn-login" onClick={this.props.HandleOnSubmit}>
                                    Reset Password
                                </button>
                                <p>
                                        <a onClick={()=>HandlePageOnCLick(this.props,'/')} className="s-btn-link">Login</a>
                                </p>
                            </div>
                        </form>
                   </div>{/* form right sec */}  
               
                </div>
            </React.Fragment>
        )
    }
}

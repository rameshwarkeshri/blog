import React from 'react';
import ReactDOM from 'react-dom';
import UsersRoute from './routes/users-routes'
import * as serviceWorker from './serviceWorker';
import './assets/scss/common.scss';
import './assets/scss/media-above-1200.scss';
import './assets/scss/responsive.scss';
ReactDOM.render(
  <React.StrictMode>
    <UsersRoute/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

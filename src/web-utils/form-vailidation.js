import React, { PureComponent } from 'react';
export const FormValidation = {
    checkEmail:(email)=>{
        var emailreg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        if (emailreg.test(email)){
          return true; 
        }
        else{
                return false;
            }
    },
    // check email end
     checkNumber:(phone)=>{
          var phonereq = /^(\+\d{1,3}[- ]?)?\d{10}$/
          if(phonereq.test(phone) && phone.length == 10 ){
            return true;
          }else{
            return false;
          }
     }
    //  check phone no;
}